# CMA-web

CMA-web is a [Flask](<http://flask.pocoo.org/>) project to create a web application for the [Cestode Annotator Pipeline (CMA)](<https://github.com/machadodj/cestode_mitogenome_annotator>).

You should make changes on the Python application in `template.py` and execute it locally using `execute_flask.sh`.

By default, the Flask site should be at <http://127.0.0.1:5000/> wille app.py is running, but see messages on your screen.

You can visit this address using any internet browser.