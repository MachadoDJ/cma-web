#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# Import modules (requires flask and wtforms)
##

import os, re, sys, string, random
from datetime             import datetime
from flask                import Flask, flash, request, redirect, render_template, url_for, send_file, session
from random               import randint
from subprocess           import Popen, PIPE, STDOUT
from werkzeug.utils       import secure_filename
from wtforms              import Form, TextField, TextAreaField, validators, StringField, SubmitField

##
# Set upload folder and white list of file extensions
##

UPLOAD_FOLDER      = "./uploads" # Change this line accordingly
MAX_CONTENT_PATH   = "50000"     # Maximum upload volume: 50 KB
ALLOWED_EXTENSIONS = set(["fasta", "fas", "fa"]) # Only these file extensions are allowed

##
# Create app
##

DEBUG = True
app   = Flask(__name__)
app.config.from_object(__name__)
app.config["SECRET_KEY"]         = "XXXXXXXXXXXXXXXXXXXXXXX"
app.config["UPLOAD_FOLDER"]      = UPLOAD_FOLDER
app.config["MAX_CONTENT_PATH"]   = MAX_CONTENT_PATH

##
# Form variables
##

class ReusableForm(Form):
    name   = TextField("Name",  validators=[validators.required()])
    email  = TextField("Email", validators=[validators.required()])

##
# Functions
##

def get_jobid():
    while True:
        jobid     = "".join(random.choices(string.ascii_letters + string.digits, k=8))
        if not os.path.isfile("jobs/{}.txt".format(jobid)):
            break
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")
    return jobid, timestamp

def write_to_disk(name, email, fasta):
    jobid, timestamp = get_jobid()
    data = open("app.log", "a")
    data.write("Timestamp={},Name={},Email={},Fasta={},JobID={}\n".format(timestamp, name, email, fasta, jobid))
    data.close()
    return jobid

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

##
# Routes
##

@app.route("/", methods=["GET", "POST"])
def hello():
    fasta = ""
    form = ReusableForm(request.form)
    if request.method == 'POST':
        name  = request.form['name']
        session['name'] = name
        email = request.form['email']
        session['email'] = email
        if 'fasta' not in request.files:
            flash('Error 0: An input file in FASTA format is need.')
            return redirect(request.url)
        fasta = request.files['fasta']
        if fasta.filename == '':
            flash("Error 1: Empty input file.")
            return redirect(request.url)
        if not allowed_file(secure_filename(fasta.filename)):
            flash("Error 2: The input file must end with '.fasta,' '.fas,' or '.fa.'")
            return redirect(request.url)
        else:
            if form.validate():
                flash("Form completed. Please wait.")
                jobid = write_to_disk(name, email, secure_filename(fasta.filename))
                session['fasta'] = secure_filename(fasta.filename)
                session['jobid'] = jobid
                fasta.save("{}/{}.fasta".format(UPLOAD_FOLDER, jobid))
                flash("name = {}; email = {}; jobid = {}.".format(name, email, jobid))
                return render_template("loading.html")
            else:
                flash("Error 3: All the form fields are required.")
                return redirect(request.url)
    return render_template("index.html", form=form)

@app.route("/loading/", methods=['GET', 'POST'])
def loading():
    myjobid = session['jobid']
    cmd = "bash execute_cma.sh uploads/{0}.fasta {0}".format(jobid)
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT)
    p.wait()
    stdout, stderr = p.communicate()
    handle = open("stdout.txt", "w")
    handle.write(str(stdout, "utf-8"))
    handle.close()
    handle = open("stderr.txt", "w")
    handle.write(str(stderr))
    handle.close()
    try:
        return render_template("downloads.html", value=myjobid)
    except Exception as e:
        return str(e)

@app.route("/return-files/", methods=['GET', 'POST'])
def return_files_tut():
    try:
        form = ReusableForm(request.form)
        if request.method == 'POST':
			jobid = request.form.get['myjobid']
			path_to_download_file="jobs/{}.tgz".format(str(jobid))
			download_file_name="{}.tgz".format(str(jobid))
			return send_file(path_to_download_file, as_attachment=True, attachment_filename=download_file_name)
    except Exception as e:
        return str(e)

##
# Run the app
##

if __name__ == '__main__':
    # app.run()
        app.run(host='0.0.0.0')
