#!/usr/bin/env bash
set -o errexit

# execute_flast.sh
# usage: bash execute_flast.sh
# This script should be executed by crontabs at system's start up

##
# Define functions
##

function change_mod {
	chmod +rx-w app.py
}

function run_flask {
	flask run --host='0.0.0.0' > flask.out 2> flask.err &
}

##
# Execute functions
##

# change_mod
run_flask

exit
