#!/usr/bin/env bash
set -o errexit

##
# execute_cma.sh
# usage: bash execute_cma.sh <input_file> <code>
##

##
# Make sure cma.py is on system's PATH
##

export CMA_HOME="/home/fernando/sw/CMA" # CMA_HOME must match the installation of CMA in your system
export PATH="${PATH}:${CMA_HOME}"
echo "PATH=${PATH}"

##
# Working directory where the genome is and where the output directory will be created
##

export CurDir="`pwd`"
export WD="/home/fernando/sw/CMA-Web/jobs" # You must have a 'jobs' directory set in your system

##
# Handle input arguments
##

if [ "$#" -ne 2 ] ; then
	echo "Error: Illegal number of parameters (requires 2, $# given)."
	exit
fi

export GENOME=$1
export OUT=$2

if [ ! -f "${GENOME}" ] ; then
	echo "Error: File ${GENOME} was not found."
	exit
else
	echo "Input file is ${GENOME}"
fi

##
# Define functions
##

function cma {
	cd "${WD}"
	echo "Working directory = `pwd`"
	echo "Project ID = ${OUT}"
	echo 'Command line: cma.py -g "${OUT}/genome.fasta" -c "${CMA_HOME}/configuration.txt" -d "${OUT}" -v'
	cma.py -g "${OUT}/genome.fasta" -c "${CMA_HOME}/configuration.txt" -l "${CMA_HOME}/gene_lengths.txt" -d "${OUT}" -v
	wait
	mv output.gff ${OUT} ; tar -zcvf ${OUT}.tgz ${OUT} ; rm -r ${OUT}
	wait
	cd ${CurDir}
}

function main {
	# bash generate random 8 character alphanumeric string (lowercase only)
	while true ; do
		if ! [ -d "${WD}/${OUT}" ] ; then
			mkdir ${WD}/${OUT}
			cp "${GENOME}" "${WD}/${OUT}/genome.fasta"
			cma
			break
		fi
		done
}

##
# Queue control
##

current=$$ # PID of the current script
x=0 # A simple counter


# 1. List all CMA scripts already running

for i in `ps aux | grep 'execute_cma.sh' | grep 'bash' | grep -v '/bin/sh'  | awk '{print $2}'` ; do
		if [ "${current}" -ne "${i}" ] ; then
			q[${x}]=${i} #  q will list all other PIDs for CMA Bash scripts
			x=$(( ${x} + 1 )) # x counts the humber of PIDs in q
		fi
	done

echo "> id=${OUT}: pid=${current}: q=${q[@]}: x=${x}" >> execute_cma.log

# 2. Wait until they are all done

if [ "${x}" -ne "0" ] ; then # Go ahead if x equals zero
	while [ "${x}" -ne "0" ] ; do # Wait in line otherwise
		echo "-- ${OUT}: waiting ${x}" >> execute_cma.log
		sleep ${x}
		y=0 # An internal counter
		for i in `ps aux | grep 'execute_cma.sh' | grep 'bash' | grep -v '/bin/sh' | awk '{print $2}'` ; do
			for j in ${q[@]} ; do
				if [ "${current}" -ne "${i}" ] ; then
					if [ ${j} == ${i} ] ; then
						y=$(( ${y} + 1 ))
					fi
				fi
			done
		done
		x=${y}
	done
fi

echo "-- ${OUT}: running" >> execute_cma.log

##
# Execute functions
##

STARTTIME=$(date +%s)
main
ENDTIME=$(date +%s)

echo "Done in $(( ${ENDTIME} - ${STARTTIME} )) seconds."

exit
